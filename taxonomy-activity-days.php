<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
function cmp($a, $b)
{
    if ($a['date'] == $b['date']) {
        return 0;
    }
    return ($a['date'] < $b['date']) ? -1 : 1;
}

get_header();


$description = get_the_archive_description();
$activity_day_id = get_queried_object()->term_id;
?>
<!-- Je suis taxo activity days -->

<div class="program-head">
	<img src="/wp-content/themes/territoires-sauvages/assets/images/FESTIVAL.svg" alt="">
	<h1>LE PROGRAMME DU FESTIVAL</h1>
</div>

<?php if ( have_posts() ) : ?>

	<?php get_template_part( 'template-parts/nav-programme' ); ?>

	<header class="wp-block-cover alignfull has-transparent-background-color has-background-dim archive-header activity-header days-header">
		<div class="wp-block-cover__inner-container">
			<?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
			<?php if ( $description ) : ?>
				<div class="archive-description"><?php echo wp_kses_post( wpautop( $description ) ); ?></div>
			<?php endif; ?>
		</div>
	</header>


	<div class="default-max-width activity-days-grid">
		<?php 
		$posts_day = array();
		while ( have_posts() ) :
			the_post(); 
			if( have_rows('sessions') ):
				while( have_rows('sessions') ) : the_row();
					if(get_field("date", "activity-days_".$activity_day_id) == get_sub_field('day')){
						$session = get_sub_field('begin').' - '.get_sub_field('end');
						
						$posts_day[] = array(
							"id"		=> get_the_id(), 
							"date"		=> (int)str_replace(":","",get_sub_field('begin')),
							"session" 	=> $session,
							"status"	=> get_sub_field('status')
						);
					}
				endwhile;
			endif;
		endwhile; 

		if ( $posts_day ) :
			usort($posts_day, "cmp");
			foreach($posts_day as $postDay):
				$post = get_post($postDay["id"]);
				setup_postdata( $post );
				set_query_var( 'session', $postDay["session"] );
				set_query_var( 'status', $postDay["status"] );
				get_template_part( 'template-parts/content/content', "day" );
			endforeach;
			wp_reset_postdata();
		endif;

		?>
	</div>

<?php endif; ?>

<?php get_footer(); ?>
