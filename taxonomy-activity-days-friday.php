<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();

$description = get_the_archive_description();
$activity_categories_id = get_queried_object()->term_id;
?>
<div class="program-head">
	<img src="/wp-content/themes/territoires-sauvages/assets/images/FESTIVAL.svg" alt="">
	<h1>LE PROGRAMME DU FESTIVAL</h1>
</div>

<?php if ( have_posts() ) : ?>

	<?php get_template_part( 'template-parts/nav-programme' ); ?>

	<header class="wp-block-cover alignfull has-transparent-background-color has-background-dim archive-header activity-header days-header">
		<div class="wp-block-cover__inner-container">
			<?php // the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
			<h1 class="page-title">Soirée d'ouverture</h1>
			<?php if ( $description ) : ?>
				<div class="archive-description"><?php echo wp_kses_post( wpautop( $description ) ); ?></div>
			<?php endif; ?>
		</div>
	</header>


	<div class="default-max-width">
		<?php 
		while ( have_posts() ) : 
				the_post();
				$term = get_the_terms($post->ID, "activity-categories");
				if(isset($term[0])){
					get_template_part( 'template-parts/content/content', $term[0]->slug );
				}
		endwhile; ?>
	</div>


	<nav class="alignwide blog-pagination">
		<div class="alignleft"><?php previous_posts_link('&laquo; Page précédente') ?></div>
		<div class="alignright"><?php next_posts_link('Page suivante &raquo;','') ?></div>
	</nav>

<?php else : ?>
	<?php get_template_part( 'template-parts/content/content-none' ); ?>
<?php endif; ?>

<?php get_footer(); ?>
