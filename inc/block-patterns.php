<?php
/**
 * Block Patterns
 *
 * @link https://developer.wordpress.org/reference/functions/register_block_pattern/
 * @link https://developer.wordpress.org/reference/functions/register_block_pattern_category/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

/**
 * Register Block Pattern Category.
 */
/*if ( function_exists( 'register_block_pattern_category' ) ) {

	register_block_pattern_category(
		'ihag',
		array( 'label' => esc_html__( 'ihag', 'ihag' ) )
	);
}*/

/**
 * Register Block Patterns.
 */
/*if ( function_exists( 'register_block_pattern' ) ) {
$serv_name = 'https://'.$_SERVER['SERVER_NAME'];
	// Large Text.
	register_block_pattern(
		'greenmetrics/member',
		array(
			'title'         => esc_html__( 'Mission', 'ihag' ),
			'categories'    => array( 'ihag' ),
			//'viewportWidth' => 1440,
			'content'       => '<!-- wp:columns {"verticalAlignment":"center"} -->
			<div class="wp-block-columns are-vertically-aligned-center"><!-- wp:column {"verticalAlignment":"center"} -->
			<div class="wp-block-column is-vertically-aligned-center"><!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"2.25rem"}},"textColor":"green"} -->
			<p class="has-text-align-center has-green-color has-text-color" style="font-size:2.25rem">[wp-svg-icons custom_icon="Icon-ionic-ios-chatbubbles" wrap="i"]</p>
			<!-- /wp:paragraph -->
			
			<!-- wp:heading {"textAlign":"center","style":{"typography":{"fontSize":"24px"}}} -->
			<h2 class="has-text-align-center" style="font-size:24px">Un besoin ? Une question ?</h2>
			<!-- /wp:heading -->
			
			<!-- wp:buttons {"contentJustification":"center"} -->
			<div class="wp-block-buttons is-content-justification-center"><!-- wp:button -->
			<div class="wp-block-button"><a class="wp-block-button__link">Nous contacter</a></div>
			<!-- /wp:button --></div>
			<!-- /wp:buttons --></div>
			<!-- /wp:column -->
			
			<!-- wp:column {"verticalAlignment":"center"} -->
			<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"id":2577,"sizeSlug":"full","linkDestination":"none"} -->
			<figure class="wp-block-image size-full"><img src="/wp-content/themes/greenmetrics/assets/images/lorem.jpg" alt="" class="wp-image-2577"/></figure>
			<!-- /wp:image --></div>
			<!-- /wp:column --></div>
			<!-- /wp:columns -->',
		)
	);

	register_block_pattern(
		'greenmetrics/partenaires',
		array(
			'title'         => esc_html__( 'Partenaires', 'ihag' ),
			'categories'    => array( 'ihag' ),
			//'viewportWidth' => 1440,
			'content'       => '<!-- wp:paragraph {"align":"center","style":{"typography":{"fontSize":"24px"}}} -->
			<p class="has-text-align-center" style="font-size:24px"><strong>Ils nous soutiennent </strong></p>
			<!-- /wp:paragraph -->
			
			<!-- wp:columns {"align":"wide","className":"mobile-unshown"} -->
			<div class="wp-block-columns alignwide mobile-unshown"><!-- wp:column {"verticalAlignment":"center"} -->
			<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"id":190,"sizeSlug":"150-150","linkDestination":"none"} -->
			<figure class="wp-block-image size-150-150"><img src="/wp-content/themes/greenmetrics/assets/images/lorem.jpg" alt="" class="wp-image-190"/></figure>
			<!-- /wp:image --></div>
			<!-- /wp:column -->
			
			<!-- wp:column {"verticalAlignment":"center"} -->
			<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"id":192,"sizeSlug":"150-150","linkDestination":"none"} -->
			<figure class="wp-block-image size-150-150"><img src="/wp-content/themes/greenmetrics/assets/images/lorem.jpg" alt="" class="wp-image-192"/></figure>
			<!-- /wp:image --></div>
			<!-- /wp:column -->
			
			<!-- wp:column {"verticalAlignment":"center"} -->
			<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"id":193,"sizeSlug":"150-150","linkDestination":"none"} -->
			<figure class="wp-block-image size-150-150"><img src="/wp-content/themes/greenmetrics/assets/images/lorem.jpg" alt="" class="wp-image-193"/></figure>
			<!-- /wp:image --></div>
			<!-- /wp:column -->
			
			<!-- wp:column {"verticalAlignment":"center"} -->
			<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"id":194,"sizeSlug":"150-150","linkDestination":"none"} -->
			<figure class="wp-block-image size-150-150"><img src="/wp-content/themes/greenmetrics/assets/images/lorem.jpg" alt="" class="wp-image-194"/></figure>
			<!-- /wp:image --></div>
			<!-- /wp:column -->
			
			<!-- wp:column {"verticalAlignment":"center"} -->
			<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"id":195,"sizeSlug":"150-150","linkDestination":"none"} -->
			<figure class="wp-block-image size-150-150"><img src="/wp-content/themes/greenmetrics/assets/images/lorem.jpg" alt="" class="wp-image-195"/></figure>
			<!-- /wp:image --></div>
			<!-- /wp:column -->
			
			<!-- wp:column {"verticalAlignment":"center"} -->
			<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"id":196,"sizeSlug":"150-150","linkDestination":"none"} -->
			<figure class="wp-block-image size-150-150"><img src="/wp-content/themes/greenmetrics/assets/images/lorem.jpg" alt="" class="wp-image-196"/></figure>
			<!-- /wp:image --></div>
			<!-- /wp:column -->
			
			<!-- wp:column {"verticalAlignment":"center"} -->
			<div class="wp-block-column is-vertically-aligned-center"><!-- wp:image {"id":191,"sizeSlug":"150-150","linkDestination":"none"} -->
			<figure class="wp-block-image size-150-150"><img src="/wp-content/themes/greenmetrics/assets/images/lorem.jpg" alt="" class="wp-image-191"/></figure>
			<!-- /wp:image --></div>
			<!-- /wp:column --></div>
			<!-- /wp:columns -->',
		)
	);

}



*/






