<?php
/**
 * Displays the post header
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

$term_slug = get_queried_object()->slug;

    echo '<div class="day-nav">';

    $days = get_terms(array('taxonomy' => 'activity-days'));

    foreach($days as $day):
    ?>
        <a class="button <?php echo($day->slug == $term_slug)?"active":"";?>" href="<?php echo get_term_link( $day );?>"><?php echo $day->name;?></a>
    <?php endforeach;
    
    echo '</div><div class="activity-nav">';

    $categories = get_terms(array('taxonomy' => 'activity-categories'));

    foreach($categories as $cat):?>
    <a href="<?php echo get_term_link( $cat );?>" class="cat <?php echo($cat->slug == $term_slug)?"active":"";?>">
        <?php
            $image = get_field('image', 'activity-categories_'.$cat->term_id);
            if( $image ) {
                $upload_dir = wp_upload_dir();
                $svgpath = str_replace($upload_dir['baseurl'],$upload_dir['basedir'],$image);
                $wp_filesystem = new WP_Filesystem_Direct(null);
                $svg = $wp_filesystem->get_contents($svgpath);
                echo $svg;
            }
        ?>
        <p><?php echo $cat->name;?></p>        
    </a>

    <?php endforeach;
?>
</div>