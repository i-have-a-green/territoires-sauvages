<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article id="activity-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="activity-content">
		<figure>
			<?php 
				//img activity
				the_post_thumbnail('200-9999');
			?>		
		</figure>
		<div class="">
			<header class="">
				<h2><?php the_title(); ?></h2>
			</header>
			<div class="activity-date">
				<?php
				if( have_rows('sessions') ):
					while( have_rows('sessions') ) : the_row();
						?>
						<div class="day">
							<?php the_sub_field('day'); ?> : 
							<div class="hour">
								<?php the_sub_field('begin');?> 
								- 
								<?php the_sub_field('end');?>
							</div>
							&nbsp;/&nbsp;
						</div>
					<?php
					endwhile;
				endif; ?>
			</div>
			<div class="primary-content">
				<?php
				the_content();
				?>
			</div>
		</div>
		<?php 
		$term = get_the_terms($post->ID, "activity-categories");
		if(isset($term[0])){
			$image = get_field('image', 'activity-categories_'.$term[0]->term_id);
			if( $image ) {
				$upload_dir = wp_upload_dir();
				$svgpath = str_replace($upload_dir['baseurl'],$upload_dir['basedir'],$image);
				$wp_filesystem = new WP_Filesystem_Direct(null);
				$svg = $wp_filesystem->get_contents($svgpath);
				echo $svg;
				}
			}
		?>
		
	</div>
		
		<!--  -->
	<div class="real-container">
		<?php
			if( have_rows('filmmakers') ):
				while( have_rows('filmmakers') ) : the_row();
					?>
					<div class="real">
						<?php
						$image = get_sub_field('image');
						$size = '200-9999'; // (thumbnail, medium, large, full or custom size)
						if( $image ) {
							echo wp_get_attachment_image( $image, $size );
						}
						?>
						<div class="real-content">
							<div class="name">
							<?php
							the_sub_field('title');
							?>
							</div>
							<?php the_sub_field('content');?>
						</div>
					</div>
					<?php
				endwhile;
			endif;
		?>		
	</div>

	<!-- .entry-content -->
	<?php if($status != "cancel"):?>
		<?php if(get_field('free_access')):?>
			<a href="<?php the_field("hello_asso", "options");?>" target="_blank">Je m'inscris</a>
		<?php else:?>
			<a href="#">Accès libre</a>
		<?php endif;?>
	<?php endif;?>
	


</article><!-- #post-<?php the_ID(); ?> -->
