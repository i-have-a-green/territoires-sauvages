<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article id="activity-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header single-header alignwide">
			<?php the_title( '<h2 >', '</h2>' ); ?>
			<?php 
				//img activity
				ihag_post_thumbnail(); 
				
				//picto category
				$term = get_the_terms($post->ID, "activity-categories");
				if(isset($term[0])){
					$image = get_field('image', 'activity-categories_'.$term[0]->term_id);
				if( $image ) {
					$upload_dir = wp_upload_dir();
					$svgpath = str_replace($upload_dir['baseurl'],$upload_dir['basedir'],$image);
					$wp_filesystem = new WP_Filesystem_Direct(null);
					$svg = $wp_filesystem->get_contents($svgpath);
					echo $svg;
					}
				}
			
			?>

	</header><!-- .entry-header -->

	<div class="entry-content alignwide">
		<?php
		if( have_rows('sessions') ):
			while( have_rows('sessions') ) : the_row();
		
				the_sub_field('day');
				the_sub_field('begin');
				the_sub_field('end');

			endwhile;
		endif;

		the_content();

		the_field("website");
		?>
	</div><!-- .entry-content -->
	<?php if($status != "cancel"):?>
		<?php if(get_field('free_access')):?>
			<a href="<?php the_field("hello_asso", "options");?>" target="_blank">Je m'inscris</a>
		<?php else:?>
			<a href="#">Accès libre</a>
		<?php endif;?>
	<?php endif;?>

	


</article><!-- #post-<?php the_ID(); ?> -->
