<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class();?> >


<div class="activity-content">
		<figure>
			<?php 
				//img activity
				the_post_thumbnail('200-9999');
			?>		
		</figure>
		<div class="">
			<header class="">
				<h2><?php the_title(); ?> - <?php the_field("age");?> - <?php the_field("during");?></h2>
			</header>

			<div class="primary-content">
				<?php
				the_content();
				?>
			</div>
		</div>
		<?php 
		$term = get_the_terms($post->ID, "activity-categories");
		if(isset($term[0])){
			$image = get_field('image', 'activity-categories_'.$term[0]->term_id);
			if( $image ) {
				$upload_dir = wp_upload_dir();
				$svgpath = str_replace($upload_dir['baseurl'],$upload_dir['basedir'],$image);
				$wp_filesystem = new WP_Filesystem_Direct(null);
				$svg = $wp_filesystem->get_contents($svgpath);
				echo $svg;
				}
			}
		?>
		
	</div>
	<?php if($status != "cancel"):?>
		<?php if(get_field('free_access')):?>
			<a href="<?php the_field("hello_asso", "options");?>" target="_blank">Je m'inscris</a>
		<?php else:?>
			<a href="#">Accès libre</a>
		<?php endif;?>
	<?php endif;?>
</article><!-- #post-<?php the_ID(); ?> -->
