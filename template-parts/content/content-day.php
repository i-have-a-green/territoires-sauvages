<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>

<article id="activity-<?php the_ID(); ?>" <?php post_class("activity-status-".$status); ?>>
	<div class="content">
		<?php the_title( '<h2 >', '</h2>' ); ?>
		<?php 
			//picto category
			$term = get_the_terms($post->ID, "activity-categories");
			if(isset($term[0])){
				$image = get_field('image', 'activity-categories_'.$term[0]->term_id);
				$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
				if( $image ) {
					$upload_dir = wp_upload_dir();
					$svgpath = str_replace($upload_dir['baseurl'],$upload_dir['basedir'],$image);
					$wp_filesystem = new WP_Filesystem_Direct(null);
					$svg = $wp_filesystem->get_contents($svgpath);
					echo $svg;
				}
			}
		?>
		<div class="hour">
			<?php echo $session;?>
		</div>
		<div class="age">
			<?php 
				if(!empty(get_field("age"))){
					the_field("age");
				}
			?>
		</div>
		<div class="activity-helper">
			<?php
				if(!empty(get_field("accompanying"))){
					?>
					<p class="bold">Accompagnateur :</p>
					<?php
					the_field("accompanying");
				}
				if( have_rows('filmmakers') ):
					while( have_rows('filmmakers') ) : the_row();
						?>
							<div class="name">
								<p class="bold">Réalisateur :</p>	
								<?php
								the_sub_field('title');
								?>
							</div>
						<?php
					endwhile;
				endif;
			?>
		</div>
	</div>
	<?php
		$link = '';
		if(isset($term[0])){
			$link = get_term_link($term[0], "activity-categories").'#activity-'.$post->ID;
		}
	?>
	<div class="links">
		<div class="more">
			<a href="<?php echo $link;?>">En savoir plus</a>
		</div>
		<div class="inscription">
			<?php if($status != "cancel"):?>
				<?php if(get_field('free_access')):?>
					<a href="<?php the_field("hello_asso", "options");?>" target="_blank">Je m'inscris</a>
				<?php else:?>
					<a href="#">Accès libre</a>
				<?php endif;?>
			<?php endif;?>
		</div>		
	</div>

	
</article><!-- #post-<?php the_ID(); ?> -->
