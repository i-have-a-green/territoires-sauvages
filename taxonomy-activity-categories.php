<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

get_header();


$activity_categories_id = get_queried_object()->term_id;
?>
<!-- Je suis taxo activity days -->

<div class="program-head">
	<img src="/wp-content/themes/territoires-sauvages/assets/images/FESTIVAL.svg" alt="">
	<h1>LE PROGRAMME DU FESTIVAL</h1>
</div>

<?php 
	if ( have_posts() ) : 

	$tax_slug = get_query_var( 'activity-categories' );
?>

	<?php get_template_part( 'template-parts/nav-programme' ); ?>

	<header class="wp-block-cover alignfull has-transparent-background-color has-background-dim archive-header activity-header cat-header">
	<div class="wp-block-cover__inner-container">
		<h1><?php the_field("title", "activity-categories_".$activity_categories_id);  ?></h1>
		<div class="archive-description"><?php the_field("content", "activity-categories_".$activity_categories_id); ?></div>
	</div>
	</header>

	<div class="default-max-width">
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php 
			if ( false === get_template_part( 'template-parts/content/content', $tax_slug )) {
				get_template_part( 'template-parts/content/content', "activity" );
			}
			?>
		<?php endwhile; ?>
	</div>


<?php endif; ?>

<?php get_footer(); ?>
