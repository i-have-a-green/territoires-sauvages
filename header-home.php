<?php
/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ihag' ); ?></a>

	<?php 
	$wrapper_classes  = 'site-header';
	$wrapper_classes .= has_custom_logo() ? ' has-logo' : '';
	$wrapper_classes .= ( true === get_theme_mod( 'display_title_and_tagline', true ) ) ? ' has-title-and-tagline' : '';
	$wrapper_classes .= has_nav_menu( 'primary' ) ? ' has-menu' : '';
	?>

	<header id="masthead" class="<?php echo esc_attr( $wrapper_classes ); ?>" role="banner">
		<div id="header-content" class="alignwide">
			<div class="site-branding">
				<div class="site-logo">
					<a href="<?php echo home_url();?>" title="<?php _e("Back to home", "ihag");?>">
						<img src="/wp-content/themes/territoires-sauvages/assets/images/LOGO BLANC.svg" alt="">
					</a>
				</div>
			</div>
			<?php get_template_part( 'template-parts/header/site-nav' ); ?>
		</div>
	</header>

	<a class="scrollTop" href="#page"></a>

	<div id="content" class="site-content">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
