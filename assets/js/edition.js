document.addEventListener('DOMContentLoaded', function() {
    if(document.getElementsByClassName("edition").length > 1){
            
        var els = document.getElementsByClassName("edition");
        
        Array.prototype.forEach.call(els, function(el) {
            els[0].classList.add("active");
            displayCarrousel(els[0]);
            carrousels = el.getElementsByClassName("carrousel");
            Array.prototype.forEach.call(carrousels, function(carrousel) {
                carrousel.id = Math.floor(Math.random() * 9999);
            });

            elsClick = el.getElementsByClassName("edition-title");
            Array.prototype.forEach.call(elsClick, function(elClick) {
                elClick.addEventListener("click", function(e) {
                    Array.prototype.forEach.call(els, function(el2) {
                        el2.classList.remove("active");    
                    });
                    e.preventDefault();
                    el.classList.add("active");
                    displayCarrousel(el);
                });
            });
        });
        

        function displayCarrousel(elem){
            carrousels = elem.getElementsByClassName("carrousel");
            Array.prototype.forEach.call(carrousels, function(carrousel) {
                jQuery( document ).ready( function( $ ) {
                    $('#'+carrousel.id+' .wp-block-group__inner-container').slick({
                        infinite: true,
                        dots: true,
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        variableWidth: true,
                        centerMode:true,
                        centerPadding: '60px',
                        responsive: [
                            {
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                }
                            },
                            {
                                breakpoint: 800,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            },
                            {
                                breakpoint: 501,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                    
                    
                });
            });
            
        } 
    }

});
        