document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("wpcf7-form");
    Array.prototype.forEach.call(els, function(el) {
        el.removeAttribute("novalidate");
    });
});